<!DOCTYPE html>
<html lang='en'>
<head>
<meta charset='UTF-8'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<title>User List</title>
</head>
<body>
<h2>User List</h2>
<table border='1'>
<tr><th>ID</th><th>Username</th><th>Password</th></tr>


<?php
$dbFile = 'users.db';

if (file_exists($dbFile)) {
    $db = new PDO('sqlite:' . $dbFile);
    $query = "SELECT * FROM users";
    $result = $db->query($query);

    foreach ($result as $row) {
        echo "<tr>";
        echo "<td>" . htmlspecialchars($row['id']) . "</td>";
        echo "<td>" . htmlspecialchars($row['username']) . "</td>";
        echo "<td>" . htmlspecialchars($row['password']) . "</td>"; // Note: Passwords should be hashed and never exposed!
        echo "<td><a href=adjustBalance.html?user=" . htmlspecialchars($row['username']) . ">Adjust balance</a></td>"; // Note: Passwords should be hashed and never exposed!
        echo "</tr>";
    }
} else {
    echo "Database file not found.";
}
?>


</table>
</body>
</html>
