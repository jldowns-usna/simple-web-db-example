<?php
header('Content-Type: application/json');

$dbFile = '../users.db';

if (file_exists($dbFile)) {
    $db = new PDO('sqlite:' . $dbFile);
    $query = "SELECT * FROM users";
    $result = $db->query($query);
    
    $users = [];
    foreach ($result as $row) {
        $users[] = [
            'id' => $row['id'],
            'username' => $row['username'],
            // Do not send passwords, even hashed, for real applications
            'password' => $row['password']
        ];
    }

    echo json_encode($users);
} else {
    echo json_encode(['error' => 'Database file not found.']);
}
?>
