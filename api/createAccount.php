<?php

// But this is a relative path to 
$dbFile = '../users.db';


// Check if the SQLite database file exists; if not, create it and initialize the users table.
if (!file_exists($dbFile)) {
    $db = new PDO('sqlite:' . $dbFile);
    $db->exec("CREATE TABLE users (id INTEGER PRIMARY KEY, username TEXT, password TEXT)");
} else {
    $db = new PDO('sqlite:' . $dbFile);
}

$username = $_POST['username'];
$password = $_POST['password']; // In a real-world application, ensure you hash the password!

try {
    $stmt = $db->prepare("INSERT INTO users (username, password) VALUES (?, ?)");
    $stmt->execute([$username, $password]);

    // Redirect to success page on successful insertion.
    // I'm using an absolute path here
    header("Location: ../success.html?username=" . urlencode($username));
    exit();
} catch (Exception $e) {
    // Redirect to failure page on error
    header("Location: $BASEURL/failure.html");
    exit();
}
?>
