<?php

header('Content-Type: application/json');

$dbFile = '../users.db';
$db = new PDO('sqlite:' . $dbFile);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$username = $_GET['user'] ?? '';
$change = $_GET['change'] ?? null;

if (!empty($username) && isset($change)) {
    // Update balance
    $stmt = $db->prepare("UPDATE users SET AccountBalance = AccountBalance + :change WHERE username = :username");
    $stmt->execute([':change' => $change, ':username' => $username]);
}

// Fetch the updated/current balance
$stmt = $db->prepare("SELECT AccountBalance FROM users WHERE username = :username");
$stmt->execute([':username' => $username]);
$balance = $stmt->fetchColumn();

echo json_encode(['balance' => $balance]);
?>
