<?php

$dbFile = '../users.db';

$db = new PDO('sqlite:' . $dbFile);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Add the AccountBalance column with a default value of 0
$db->exec("ALTER TABLE users ADD COLUMN AccountBalance INTEGER DEFAULT 0");

echo "Database schema updated successfully.";
?>